import * as React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

export function LoginScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Yolo System</Text>
      <TextInput
        style={styles.textInput}
        placeholder="Tên đăng nhâp"
        placeholderTextColor="grey"
      />
      <TextInput
        style={styles.textInput}
        placeholder="Mật khẩu"
        placeholderTextColor="grey"
      />
      <TouchableOpacity style={{...styles.button, backgroundColor: 'purple'}}>
        <Text>Login</Text>
      </TouchableOpacity>
      <Text style={styles.title}>Or</Text>
      <TouchableOpacity style={[styles.button, {backgroundColor: 'green'}]}>
        <Text>Facebook</Text>
      </TouchableOpacity>
      <TouchableOpacity style={[styles.button, styles.buttonFacebook]}>
        <Text>Google</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#312e38',
    flexDirection: 'column',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 48,
    color: 'green',
  },
  textInput: {
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: 'gray',
    height: 40,
    paddingHorizontal: 8,
    marginVertical: 5,
    width: '90%',
    color: 'white',
  },

  button: {
    width: '90%',
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    marginVertical: 10,
  },

  buttonFacebook: {
    backgroundColor: 'red',
  },
});
