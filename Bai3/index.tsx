import * as React from 'react';
import {useState} from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

export function Bai3() {
  const [number, setNumber] = useState(0);

  console.log(`Bai3 ${number}`);
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Bai3</Text>
      <Text style={{color: 'white', fontSize: 20, margin: 20}}>{number}</Text>
      <Text style={{color: 'white', fontSize: 20, margin: 20}}>
        {number % 2 === 0 ? 'Chẵn' : 'Lẻ'}
      </Text>
      <Button
        title="Increment number"
        onPress={() => {
          setNumber(number + 1);
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#312e38',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 22,
    color: '#fff',
  },
});
