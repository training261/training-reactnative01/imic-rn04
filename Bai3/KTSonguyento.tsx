import * as React from 'react';
import {useState} from 'react';
import {View, Text, StyleSheet, TextInput, Button} from 'react-native';

function kiem_tra_snt(n: number) {
  // Biến cờ hiệu
  var flag = true;

  // Nếu n bé hơn 2 tức là không phải số nguyên tố
  if (n < 2) {
    flag = false;
  } else if (n === 2) {
    flag = true;
  } else if (n % 2 === 0) {
    flag = false;
  } else {
    // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
    for (var i = 3; i < Math.sqrt(n); i += 2) {
      if (n % i === 0) {
        flag = false;
        break;
      }
    }
  }

  // Kiểm tra biến flag
  return flag;
}

export function KTSonguyento() {
  const [number, setNumber] = useState('');
  const [checkResult, setCheckResult] = useState('');
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{checkResult}</Text>
      <TextInput
        value={number}
        onChangeText={setNumber}
        style={styles.textInput}
        placeholder="Nhập số"
        placeholderTextColor="grey"
        returnKeyType={'done'}
        keyboardType="number-pad"
        inputMode="numeric"
      />
      <Button
        title="Kiểm tra số nguyên tố"
        onPress={() => {
          const isPrimitiveNumber = kiem_tra_snt(+number);
          if (isPrimitiveNumber) {
            setCheckResult(`Số ${number} là số nguyên tố`);
          } else {
            setCheckResult(`Số ${number} không phải là số nguyên tố`);
          }
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#312e38',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 22,
    color: '#fff',
  },
  textInput: {
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: 'gray',
    height: 40,
    paddingHorizontal: 8,
    marginVertical: 5,
    width: '90%',
    color: 'white',
  },
});
