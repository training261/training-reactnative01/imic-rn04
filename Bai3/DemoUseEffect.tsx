import * as React from 'react';
import {useCallback, useEffect, useMemo, useState} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

export function DemoUseEffect() {
  const [number, setNumber] = useState(0);
  const [number2, setNumber2] = useState(0);
  useEffect(() => {
    console.log('DemoUseEffect');
  }, []);
  useEffect(() => {
    console.log('DemoUseEffect changed state ' + number);
  }, [number, number2]);
  let memoizedValue = useMemo(() => computeExpensiveValue(number2), [number2]);
  function a() {
    console.log('a');
  }
  let memoizedCallback1 = useCallback(() => {
    console.log('Demo use callback');
    setNumber(number + 1);
  }, [number]);
  let memoizedCallback = useCallback(() => {
    console.log('memoizedCallback');
  }, []);
  useEffect(() => {
    memoizedCallback();
  }, [memoizedCallback]);

  useEffect(() => {
    a();
  }, [a]);

  console.log(`render: ${number}`);
  return (
    <View style={styles.container}>
      <Text style={styles.title}>DemoUseEffect</Text>
      <Text style={{color: 'white', fontSize: 20, margin: 20}}>{number}</Text>
      <Text style={{color: 'white', fontSize: 20, margin: 20}}>
        {number % 2 === 0 ? 'Chẵn' : 'Lẻ'}
      </Text>
      <Text>{memoizedValue}</Text>
      <Button title="Increment number" onPress={memoizedCallback1} />
      <Button
        title="Increment number 2"
        onPress={() => {
          setNumber2(number2 + 1);
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#312e38',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 22,
    color: '#fff',
  },
});
function computeExpensiveValue(deps: number): any {
  console.log('computeExpensiveValue');
  return new Array(deps).fill(0);
}
